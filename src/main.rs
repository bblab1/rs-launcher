extern crate clap;
extern crate chrono;
extern crate glob;
extern crate libc;

use clap::{Arg, App};

use std::fs;
use std::path::Path;
use std::thread;
use std::time::{SystemTime, UNIX_EPOCH};
use std::time;
use std::os::unix::process::CommandExt;
use std::process::Command;

use chrono::prelude::*;
use glob::glob;

fn now_sec() -> u64 {
    SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_secs()
}

fn run_simple_command_async(cmd_str : String) {
    println!("run simple command {}", cmd_str);
    let cmds : Vec<&str> = cmd_str.split_whitespace().collect();
    Command::new(cmds[0])
        .args(&cmds[1..])
        .spawn()
        .expect("failed to start command");
}

fn archive_logs() {
    for entry in glob("./**/*.log").expect("failed to read the glob pattern") {
        match entry {
            Ok(path) => {
                run_simple_command_async(format!("gzip {}", path.to_str().expect("failed to convert path to string")));
            },
            Err(e) => {
                println!("glob error {:?}", e);
            }
        }
    }
}

struct RunningCommand {
    child : std::process::Child,
    is_alive : bool,
}

fn run_command(command : &String) -> RunningCommand {
    archive_logs();

    let today = Local::now().format("%Y-%m-%d").to_string();

    if !Path::new(&today).exists() {
        fs::create_dir(&today).expect(&format!("failed to create subfolder {}", today)[..]);
    }

    let cmds : Vec<&str> = command.split_whitespace().collect();
    let child = Command::new(cmds[0])
        .args(&cmds[1..])
        .current_dir(today)
        .before_exec(|| {
            unsafe {
                let _ret_code = libc::prctl(libc::PR_SET_PDEATHSIG, libc::SIGHUP);
                // println!("prctl returns {}", _ret_code);
            }
            Ok(())
        })
        .spawn()
        .expect("failed to start command");
    println!("command started");

    RunningCommand {
        child : child,
        is_alive : true,
    }
}

fn main() {
    let matches = App::new("laucher")
        .version("1.0")
        .author("hl")
        .about("laucher the process")
        .arg(Arg::with_name("restart_sec")
              .short("s")
              .long("restart_sec")
              .value_name("SECONDS")
              .help("restart the command by SECONDS")
              .takes_value(true))
        .arg(Arg::with_name("command")
              .short("c")
              .long("command")
              .value_name("COMMAND")
              .required(true)
              .help("the command to launch"))
        .get_matches();

    let restart_sec = matches.value_of("restart_sec").unwrap_or("");
    let command = String::from(matches.value_of("command").unwrap());

    println!("launcher: will launch command '{}', restart_sec '{}'", command, restart_sec);

    let restart_sec : u64 = restart_sec.parse().unwrap_or(0);

    let mut last_timeslot = 0;

    if restart_sec != 0 {
        last_timeslot = now_sec() / restart_sec;
        println!("  and will restart it every {} seconds.", restart_sec);
    }

    let mut cmd = run_command(&command);

    let sleep_interval = time::Duration::from_millis(1000);

    loop {
        if cmd.is_alive {
            match cmd.child.try_wait() {
                Ok(opt_status) => {
                    match opt_status {
                        Some(status) => {
                            if status.success() {
                                println!("command exit normally");
                            } else {
                                println!("command exit with error");
                            }
                            cmd.is_alive = false;
                        }
                        None => {
                        }
                    }
                }
                Err(e) => println!("error when attempting to wait: {}", e),
            }
        }
        if restart_sec == 0 { break; }

        thread::sleep(sleep_interval);

        // check restart time
        let curr_timeslot = now_sec() / restart_sec;

        if curr_timeslot == last_timeslot { continue; }

        last_timeslot = curr_timeslot;

        println!("launcher: restarting the command");

        if cmd.is_alive {
            cmd.child.kill().unwrap_or_else(|_e| {
                println!("launcher: cannot kill child process");
            });
            let exit_status = cmd.child.wait().expect("error when attempting to wait");
            if exit_status.success() == false {
                println!("launcher: command exit with error");
            }
            cmd.is_alive = false;
        }
        cmd = run_command(&command);
    }
}
